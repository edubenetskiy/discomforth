include parity.frt

: collatz ( n )
    dup . 32 emit
    dup 1 <> if
        0
        repeat
            drop
            dup odd? if
                3 * 1 +
            else
                2 / 
            then
            dup . 32 emit
            dup dup 1 =
        until
    else then
    drop
;
