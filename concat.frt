: dup2 ( a b -- a b a b )
    swap dup rot dup rot swap
;

: inc2 ( a b -- a+1 b+1 )
    1 + swap 1 + swap
;

: str-copy ( d s -- )
    repeat
        dup2
        c@ >r r@ swap c!
        inc2
        r> not
    until
    drop drop
;

: str-concat ( s1 s2 -- [s1 CONCAT s2] )
    ( Calculate the size of concatenated string including \0 )
    dup count rot dup count rot 1 + +       ( s2 s1 size )
    ( Allocate memory for it )
    heap-alloc              ( s2 s1 addr )
    ( Copy address to return stack )
    dup >r                  ( s2 s1 addr | addr )
    ( Copy first string )
    swap                    ( s2 addr s1 | addr )
    str-copy                ( s2 | addr )
    ( Calculate a position for the second string )
    r@ count r@ + swap      ( pos s2 | addr )
    ( Copy second string )
    str-copy                ( | addr )
    ( Return the address of the concatenated string )
    r>                      ( addr | )
;
