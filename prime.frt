(
    Returns whether a number on top of stack is prime.
    Returned value:
        0 - composite,
        1 - prime,
        2 - illegal argument
)
: prime? ( n )
    dup 0 <= if
        ." ERR: Wrong argument: Number must be positive " cr 
        drop 2
    else 
        dup 1 = if
            ." ERR: Unit is neither prime or composite " cr
            drop 2
        else
            dup 4 < if
                drop 1
            else
                0 swap
                dup 2 / 2 do
                    dup r@ %
                    if else
                        swap 1 + swap
                    then
                loop
                drop
                0 = if 1 else 0 then
            then
        then
    then
;

: prime?! ( n )
    ( Check if number is prime )
    prime?
    ( Allocate memory for one cell )
    cell% allot
    ( Place the result under the address )
    dup rot swap
    ( Write the result into memory )
    !
;
